from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every

from routes import bets
from services import bet_service

app = FastAPI()

app.include_router(bets.router)


@app.on_event("startup")
@repeat_every(seconds=60)
async def update_cash_dir_data() -> None:
    await bet_service.update_bets()


@app.on_event("startup")
@repeat_every(seconds=60)
async def update_events():
    await bet_service.update_events()
