import datetime
import aiohttp

from schemas.bet_schema import Bet
from services.managers import pg_manager, redis_manager


async def get_events() -> dict:
    events = await redis_manager.get_events()

    return events


async def bet_on_event(bet: Bet):
    if bet.sum <= 0:
        return {'response': 'not valid sum'}
    event = await redis_manager.get_event_by_id(bet.event_id)
    if event is None:
        return {'response': 'event doesnt exist'}
    if datetime.datetime.fromisoformat(event['deadline']) < datetime.datetime.now():
        return {'response': 'all bets are off'}

    if bet.bet.value == 1:
        coef = event['coefficient_1']
    elif bet.bet.value == 2:
        coef = event['coefficient_2']

    data = await pg_manager.create_bet(bet, coef)

    return data


async def get_bets(user_id: int):
    return await pg_manager.get_bets_by_user(user_id)


async def update_bets():

    async with aiohttp.ClientSession() as session:
        events = await session.get('http://line_provider:8001/get_finished_events')
        events = await events.json()
    bets = await pg_manager.get_processing_bets()

    result = []
    for event in events:
        for bet in bets:
            if bet['event_id'] == event['id']:
                if bet['bet'] != event['status']:
                    result.append((bet['id'], 2))
                else:
                    result.append((bet['id'], 1))

    await pg_manager.update_bets(result)


async def update_events():
    async with aiohttp.ClientSession() as session:
        data = await session.get('http://line_provider:8001/get_events')
        data = await data.json()
        await redis_manager.save_events(data)
