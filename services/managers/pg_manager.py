import asyncpg

from settings import PostgreSqlConfig
from schemas.bet_schema import Bet


async def connect():
    conn = await asyncpg.connect(PostgreSqlConfig.DATABASE_URL)
    return conn


async def close(conn):
    await conn.close()


async def create_bet(bet: Bet, coefficient: float) -> Bet:
    cur = await connect()

    data = await cur.fetchval("insert into bets (event_id, user_id, bet, sum, coefficient)"
                              "values ($1, $2, $3, $4, $5) returning *",
                              bet.event_id, bet.user_id, bet.bet.value, bet.sum, coefficient)
    await close(cur)
    bet.id = data
    bet.coefficient = coefficient
    return bet


async def get_bets_by_user(user_id: int) -> dict:
    cur = await connect()
    data = await cur.fetch("select * from bets where user_id = $1", user_id)

    await close(cur)
    return data


async def update_bets(result: list):
    cur = await connect()
    await cur.executemany("update bets set status = $2 where id = $1", result)
    await close(cur)


async def get_processing_bets():
    cur = await connect()

    data = await cur.fetch("select id, event_id, bet from bets where status = 0")

    await close(cur)
    return data
