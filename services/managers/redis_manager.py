import json

from aioredis import Redis, from_url

from settings import RedisConfig


async def init_redis_pool() -> Redis:
    redis = await from_url(
        RedisConfig.URL,
        encoding="utf-8",
        db=RedisConfig.DB,
        decode_responses=True,
        password=RedisConfig.PASSWORD
    )
    return redis


async def get_events():
    redis = await init_redis_pool()
    data = await redis.get('events')
    if data is None:
        return None
    return json.loads(await redis.get('events'))


async def save_events(data: dict):
    redis = await init_redis_pool()
    await redis.set('events', json.dumps(data), 60)


async def get_event_by_id(event_id: int):
    redis = await init_redis_pool()
    data = await redis.get('events')
    data = json.loads(data)
    for event in data:
        if event['id'] == event_id:
            return event
    return None
