from fastapi import APIRouter

from services import bet_service
from schemas.bet_schema import Bet

router = APIRouter()


@router.get("/events")
async def get_events():
    return await bet_service.get_events()


@router.post('/bet')
async def bet_on_event(bet: Bet):
    return await bet_service.bet_on_event(bet)


@router.get("/bets")
async def get_bets(user_id: int):
    return await bet_service.get_bets(user_id)
