create table if not exists bets
(
    id           serial
        primary key,
    event_id int,
    user_id int,
    bet int,
    sum float,
    coefficient float,
    status       integer not null default 0
);