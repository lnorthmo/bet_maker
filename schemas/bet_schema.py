import enum

from pydantic import BaseModel
from typing import Optional


class BetOnTeam(enum.Enum):
    t1_win = 1
    t2_win = 2


class Status(enum.Enum):
    live = 0
    won = 1
    lost = 2


class Bet(BaseModel):
    id: Optional[int]
    event_id: int
    user_id: int
    bet: BetOnTeam
    sum: int
    coefficient: Optional[float]
    status: Optional[Status] = 0
