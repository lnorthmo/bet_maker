class PostgreSqlConfig:
    DATABASE_URL = "postgresql://postgres:postgres@bet_maker_postgres:5432/bets"


class RedisConfig:
    URL = 'redis://bet_maker_redis:6379'
    DB = '0'
    PASSWORD = "password"
